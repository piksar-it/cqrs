<?php namespace Quasar\Cqrs;

interface CommandBus {
    public function dispatch(Command $command): void;
}
