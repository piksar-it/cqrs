<?php namespace Quasar\Cqrs;

interface CommandHandler {
    public function handle(Command $command): void;
}
