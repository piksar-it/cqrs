<?php namespace Quasar\Cqrs;

class AbstractCommand {
	
    public function __get(string $property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
}