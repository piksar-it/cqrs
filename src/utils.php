<?php namespace Quasar\Cqrs;

function dispatch(Command $command): void
{
    container()->get(CommandBus::class)->dispatch($command);
}

// function ask(\Fotocore\Shared\Domain\Bus\Query\Query $query): ?\Fotocore\Shared\Domain\Bus\Query\Response
// {
//     return container()->get(\Fotocore\Shared\Domain\Bus\Query\QueryBus::class)->ask($query);
// }
