<?php namespace Quasar\Cqrs;

final class SimpleSyncCommandBus implements CommandBus {

	private static $handlers;

    public function __construct(iterable $commandHandlers)
    {
		$this->handlers = $commandHandlers;
    }

    public function dispatch(Command $command): void
    {
        $this->handler($command)->handle($command);
    }

    private function handler(Command $command): CommandHandler
    {
    	return $this->handlers[get_class($command)];
    }
}
